import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

test_df=pd.read_csv('ecg_data/mitbih_test.csv',header=None)
X_test = test_df.iloc[:, :187].values[0:9].flatten()
X_test=X_test[X_test>0]
X_test=X_test[X_test<1]
print(X_test)

figure(num=None, figsize=(10, 3), dpi=80, facecolor='w', edgecolor='k')
plt.plot(X_test)
plt.savefig("foo.png",bbox_inches='tight')
plt.show()