from flask import jsonify, request
from flask_cors import CORS
import make_pred

from flask import Response
from flask import Flask
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

import pandas as pd
import io

app=Flask(__name__) #__name__ gives  to files an unique name
CORS(app)

@app.route('/api/prediction', methods=['POST'])
def upload_file():
    request_data=request.get_json()
    data_from_file=request_data['file']

    f = open("ecg.csv", "w+")
    f.write(data_from_file)
    f.close()

    X_test=make_pred.prepare_data_for_frediction('ecg.csv')
    pred_result=make_pred.predict(X_test)
    return jsonify({"prediction": str(pred_result), "showImg":"1"})

@app.route('/api/plot.png')
def plot_png():
    test_df = pd.read_csv('ecg.csv', header=None)
    X_test = test_df.iloc[:, :187].values[0:9].flatten()
    X_test = X_test[X_test > 0]
    X_test = X_test[X_test < 1]
    fig = create_figure(X_test)
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def create_figure(input):
    fig = Figure( figsize=(20, 5), dpi=80, facecolor='w', edgecolor='k')
    axis = fig.add_subplot(1, 1, 1)
    axis.plot(input)
    return fig



'''X_test=make_pred.prepare_data_for_frediction('ecg.csv')
pred_result=make_pred.predict(X_test)
print(pred_result)'''

'''
@app.route('/store', methods=['POST'])
def create_store():
   request_data=request.get_json()#converts a json string into a python dictionary

   new_store={
       'name':request_data['name'],
       'items':[]
   }
   stores.append(new_store)
   return jsonify(new_store)

@app.route('/store/<string:name>')
def get_store(name):
    for store in stores:
        if store['name']==name:
            return jsonify(store)
    return jsonify({"message":"store not found"})

@app.route('/store')
def get_stores():
    return jsonify({'stores':stores})#converts the stores variable into json

@app.route('/store/<string:name>/item', methods=['POST'])
def create_item_in_store(name):
    request_data = request.get_json()

    for store in stores:
        if store['name'] == name:
            new_item={
                'name':request_data['name'],
                'price':request_data['price']
            }
            stores.append(new_item)
            return jsonify(new_item)
    return jsonify({'message':'store not found'})

@app.route('/store/<string:name>/item')
def get_item_in_store(name):
    for store in stores:
        if store['name'] == name:
            return jsonify({'items':store['items']})

    return jsonify({'message':'store not found'})'''
#if _name_ == '_main_':
app.run(threaded=False,port=5000)