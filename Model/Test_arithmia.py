from keras.datasets import cifar10
from keras.utils import to_categorical
from keras.models import load_model
import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder

def two_labels(x):
    if x>0:
        return 1
    return 0

if __name__ == '__main__':
    # Load the dataset
    test_df=pd.read_csv('ecg_data/mitbih_test.csv',header=None)
    target_test = test_df[187]
    target_test = np.vectorize(two_labels)(target_test)
    print(target_test)
    y_test = to_categorical(target_test)
    X_test = test_df.iloc[:, :187].values
    X_test = np.expand_dims(X_test, 2)

    model = load_model("my_model.h5")
    scores = model.evaluate(X_test,y_test)
    model.summary()
    print('Loss: %.3f' % scores[0])
    print('Accuracy: %.3f' % scores[1])