import os
import argparse
import numpy as np
import mxnet as mx
import pandas as pd
import pickle
import logging


def add_gaussian_noise(signal):
    noise = np.random.normal(0, 0.05, 186)
    return (signal + noise)


def load_data(trainFile, testFile):
    if not os.path.isfile(trainFile) or not os.path.isfile(testFile):
        return "No Path"
    trainF = open(trainFile, "rb")
    testF = open(testFile, "rb")
    train_df = pickle.load(trainF)
    test_df = pickle.load(testF)
    trainF.close()
    testF.close()
    return train_df, test_df


def parse_data(train_df, test_df):
    if not isinstance(train_df, pd.DataFrame) or not isinstance(train_df, pd.DataFrame):
        return
    if train_df.shape[1]!=188 or test_df.shape[1]!=188:
        return
    target_train = train_df[187]
    target_test = test_df[187]
    y_train = pd.Categorical(target_train)
    y_test = pd.Categorical(target_test)

    X_train = train_df.iloc[:, :186].values
    X_test = test_df.iloc[:, :186].values
    return X_train, X_test, y_train, y_test


def prepare_data(x_train, x_test, y_train, y_test):
    if x_train.shape[0]!=y_train.shape[0] or x_test.shape[0]!=y_test.shape[0]:
        return
    for i in range(len(x_train)):
        x_train[i, :186] = add_gaussian_noise(x_train[i, :186])
    x_train = x_train.reshape(len(x_train), 1, x_train.shape[1])
    x_test = x_test.reshape(len(x_test), 1, x_test.shape[1])

    x_train = np.array(x_train)
    x_test = np.array(x_test)

    y_train = np.array(y_train)
    y_test = np.array(y_test)
    return x_train, x_test, y_train, y_test


def nn():
    data = mx.symbol.Variable('data')
    conv1 = mx.sym.Convolution(data=data, pad=1, kernel=3, num_filter=24, name="conv1", layout="NCW")
    relu1 = mx.sym.Activation(data=conv1, act_type="relu", name="relu1")
    pool1 = mx.sym.Pooling(data=relu1, pool_type="max", kernel=2, stride=2, name="max_pool1")
    # second conv layer
    conv2 = mx.sym.Convolution(data=pool1, kernel=3, num_filter=48, name="conv2", pad=(1, 1))
    relu2 = mx.sym.Activation(data=conv2, act_type="relu", name="relu2")
    pool2 = mx.sym.Pooling(data=relu2, pool_type="max", kernel=2, stride=2, name="max_pool2")

    conv3 = mx.sym.Convolution(data=pool2, kernel=5, num_filter=64, name="conv3")
    relu3 = mx.sym.Activation(data=conv3, act_type="relu", name="relu3")
    pool3 = mx.sym.Pooling(data=relu3, pool_type="max", kernel=2, stride=2, name="max_pool3")

    # conv4 = mx.sym.Convolution(data=conv3, kernel=(5,5), num_filter=64, name="conv3")
    # relu4 = mx.sym.Activation(data=conv4, act_type="relu", name="relu3")
    # pool4 = mx.sym.Pooling(data=relu4, pool_type="max", kernel=(2,2), stride=(2,2),name="max_pool3")

    # first fullc layer
    flatten = mx.sym.Flatten(data=pool3)
    fc1 = mx.symbol.FullyConnected(data=flatten, num_hidden=500, name="fc1")
    relu3 = mx.sym.Activation(data=fc1, act_type="relu", name="relu3")
    # second fullc
    fc2 = mx.sym.FullyConnected(data=relu3, num_hidden=43, name="final_fc")
    # softmax loss
    mynet = mx.sym.SoftmaxOutput(data=fc2, name='softmax')

    return mynet


def run():
    train_df, test_df = load_data("train_df.pkl", "test_df.pkl")
    x_train, x_test, y_train, y_test = parse_data(train_df, test_df)
    x_train, x_test, y_train, y_test = prepare_data(x_train, x_test, y_train, y_test)

    batch_size = 32
    train_iter = mx.io.NDArrayIter(x_train, y_train, batch_size, shuffle=True)
    val_iter = mx.io.NDArrayIter(x_test, y_test, batch_size, shuffle=True)

    print("train set : ", x_train.shape)
    print("validation set : ", x_test.shape)

    print("y train set : ", y_train.shape)
    print("y validation set :", y_test.shape)

    adam = mx.optimizer.create('adam')

    # Checkpointing (saving the model). Make sure there is folder named models exist
    # model_prefix = 'models/chkpt'
    # checkpoint = mx.callback.do_checkpoint(model_prefix)
    mynet = nn()
    # Loading the module API. Previously mxnet used feedforward (deprecated)
    model = mx.mod.Module(
        context=mx.gpu(0),  # use GPU 0 for training; if you don't have a gpu use mx.cpu()
        symbol=mynet,
        data_names=['data']
    )

    # actually fit the model for 10 epochs. Can take 5 minutes
    model.fit(
        train_iter,
        batch_end_callback=mx.callback.ProgressBar(batch_size),
        num_epoch=10,
        eval_metric='acc',
        optimizer=adam,

    )

    acc = mx.metric.Accuracy()
    model.score(val_iter, acc)
    print(acc)

    acc = mx.metric.Accuracy()
    model.score(train_iter, acc)
    print(acc)


if __name__ == "__main__":
    run()
