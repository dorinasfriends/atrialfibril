import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from keras.utils.np_utils import to_categorical
from sklearn.utils import class_weight
import warnings
warnings.filterwarnings('ignore')

def import_files(path1, path2):
    try:
        train_df=pd.read_csv(path1,header=None)
        test_df=pd.read_csv(path2,header=None)
        return  train_df,test_df
    except:
        return "Not valid path"


def classify_and_equilibre_data():
    train_df,test_df=import_files('mitbih_train.csv','mitbih_test.csv')
    train_df[187]=train_df[187].astype(int)
    equilibre=train_df[187].value_counts()
    #print(equilibre)
    plt.bar(equilibre.index,equilibre)

    from sklearn.utils import resample
    df_1=train_df[train_df[187]==1]
    df_2=train_df[train_df[187]==2]
    df_3=train_df[train_df[187]==3]
    df_4=train_df[train_df[187]==4]
    df_0=(train_df[train_df[187]==0]).sample(n=20000,random_state=42)

    df_1_upsample=resample(df_1,replace=True,n_samples=20000,random_state=123)
    df_2_upsample=resample(df_2,replace=True,n_samples=20000,random_state=124)
    df_3_upsample=resample(df_3,replace=True,n_samples=20000,random_state=125)
    df_4_upsample=resample(df_4,replace=True,n_samples=20000,random_state=126)

    train_df=pd.concat([df_0,df_1_upsample,df_2_upsample,df_3_upsample,df_4_upsample])
    return  train_df,test_df

def draw_graph():
    train_df, test_df = classify_and_equilibre_data()
    equilibre=train_df[187].value_counts()
    #print(equilibre)
    plt.bar(equilibre.index,equilibre)

    c=train_df.groupby(187,group_keys=False).apply(lambda train_df : train_df.sample(1))
    plt.plot(c.iloc[0,:186])

    def plot_hist(class_number,size,min_):
        img=train_df.loc[train_df[187]==class_number].values
        img=img[:,min_:size]
        img_flatten=img.flatten()


        final1=np.arange(min_,size)
        for i in range (img.shape[0]-1):
            tempo1=np.arange(min_,size)
            final1=np.concatenate((final1, tempo1), axis=None)
        #print(len(final1))
        #print(len(img_flatten))
        plt.hist2d(final1,img_flatten, bins=(80,80),cmap=plt.cm.jet)
        plt.show()# --uncomment
        return img

    img=plot_hist(0, 70, 5)
    plt.plot(c.iloc[1, :186])
    return img

draw_graph()




